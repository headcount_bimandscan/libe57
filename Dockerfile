#
# TEMP(neil)
#

FROM nhbs/bimandscan-debian-base2:build-1 as build_img

# Prepare environment:
RUN mkdir -p /libe57/build
COPY . /libe57/
WORKDIR /libe57/build

# Build library & tools:
RUN cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE="Release" \
                              -DCMAKE_CXX_FLAGS="-m64 -fPIC -std=gnu++14" \
                              -DCMAKE_CXX_FLAGS="-O3 -g0 -DNDEBUG" \
                              ..
RUN make -j2 && \
    make install/strip

# Final production image:
FROM nhbs/bimandscan-debian-base2:prod-1
MAINTAINER Neil Hyland <neil.hyland@bimandscan.com>

COPY --from=build_img /usr/local/bin/e57validate /usr/local/bin/

VOLUME /data
WORKDIR /data

COPY ./DockerRun.sh /DockerRun.sh
RUN chmod +x "/DockerRun.sh"

ENTRYPOINT ["bash", "-c", "/DockerRun.sh \"$@\"", "--"]
#CMD []
