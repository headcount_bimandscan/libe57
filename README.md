# E57 Import Library #

Version of the LibE57 software tools for managing E57 point cloud data files ([LINK](http://www.libe57.org/)).

See files in the 'doc' folder for installation/build instructions and old documentation.

Under Boost software licensing ('1.0').
